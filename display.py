import geopandas as gpd 
import pandas as pd
import matplotlib.pyplot as plt
import sys
import os
import hashlib

# PRA
pra = gpd.read_file("data/pra-shapefiles/pra/pra.shp")
pra.geometry = pra.geometry.to_crs(2154)
pra = pra.drop([716,717,718,719,720])

# REGIONS
reg = gpd.read_file("data/regions/Regions2016/regions-20161121.shp")
reg.geometry = reg.geometry.to_crs(2154)
reg = reg.drop([0,1,2,3,4])

aura = reg[reg.code_insee.eq("84")]
aura.loc[15].geometry
pra_aura = pra[pra.geometry.centroid.within(aura.loc[15].geometry)]

# RAM cleaning
del reg
del aura


if sys.argv[1] == "2015":
    df = pd.read_csv("results/out2015.csv")
elif sys.argv[1] == "2016":
    df = pd.read_csv("results/out2016.csv")
elif sys.argv[1] == "2018":
    df = pd.read_csv("results/out2018.csv")
elif sys.argv[1] == "2019":
    df = pd.read_csv("results/out2019.csv")

res = pra_aura.merge(df, left_on="name_pra", right_on="name_pra")

# base = pra_aura.plot(color='white', edgecolor='black')
# res.plot(ax=base, column='CODE_GROUP', categorical=True, legend=True)
# https://geopandas.org/mapping.html (Maps with Layers)

def to_hex_color(value):
    value = str(value)
    color = hashlib.sha1(value.encode("utf-8"))
    color = color.hexdigest()[0:6]
    color = "#" + color
    return color

res.plot(column='CODE_GROUP', categorical=True, legend=True, cmap='tab20c')
plt.savefig("img/pra_rpg_" + sys.argv[1] + ".png")
plt.clf()

# res["CODE_GROUP"].hist(bins=range(1,29))
# plt.show()