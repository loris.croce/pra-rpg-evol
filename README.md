# Culture types evolution by agricultural region

Add `data/` to repo's root : https://stratus.irstea.fr/d/c73a36e48b55464bafcc/

## Usage

Argument `year` = {2015,2016,2018,2019}. `.csv` results are located in [`results/`](results/) and map plots in [`img/`](img/)

### Data creation

```console
$ python3 init.py year
```

### Cultures by agricultural region

```console
$ python3 check_rpgpra.py year
```

### Map generation

```console
$ python3 init.py year
```

## Example

|           2015            |           2016            |           2018            |           2019            |
|:-------------------------:|:-------------------------:|:-------------------------:|:-------------------------:|
| ![](img/pra_rpg_2015.png) | ![](img/pra_rpg_2016.png) | ![](img/pra_rpg_2018.png) | ![](img/pra_rpg_2019.png) |

## Help

- https://gis.stackexchange.com/questions/208546/check-if-a-point-falls-within-a-multipolygon-with-python
- https://gitter.im/geopandas/geopandas?at=5f2adad8d0282f03367dcf8d
- https://gis.stackexchange.com/questions/336437/colorizing-polygons-based-on-color-values-in-dataframe-column

```
sudo pip3 uninstall shapely
sudo pip3 install shapely --no-binary shapely
sudo pip3 uninstall pygeos
sudo pip3 install pygeos --no-binary pygeos
sudo apt install libgeos-dev
sudo pip3 install pygeos --no-binary pygeos
```

## TODO 

- [ ] Create download script for data sources.
- [ ] Different level (RA, GPRA, PRA, etc.)
- [X] Map plotting
    - [X] Fix nonsense map plot :warning:
- [X] Export data results.
- [ ] Code cleaning ?

### After meeting

- 2nd culture majority
- For one group (cereal, livestock, other) get a heatmap based on [regression](https://scikit-learn.org/stable/auto_examples/linear_model/plot_ols.html) of group by PRA :
    ```
    y=a+b*t
    where t=year
    y=sum of cereal surfaces (herbs, other)
    prediction y^ in 2015 and in 2019
    computation of (y^2019-y^2015) / y^2015
    ```
- fix 2019 in [RPG graph cult](https://gitlab.irstea.fr/loris.croce/rpg_graph_cult)