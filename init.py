import geopandas as gpd
import matplotlib.pyplot as plt
import sys
import os

# PRA
pra = gpd.read_file("data/pra-shapefiles/pra/pra.shp")
pra.geometry = pra.geometry.to_crs(2154)
pra = pra.drop([716,717,718,719,720])

# REGIONS
reg = gpd.read_file("data/regions/Regions2016/regions-20161121.shp")
reg.geometry = reg.geometry.to_crs(2154)
reg = reg.drop([0,1,2,3,4])

# RPG(s)
if sys.argv[1] == "2015":
    rpg = gpd.read_file("data/rpg/2015/RPG_2-0_SHP_LAMB93_R84-2015/PARCELLES_GRAPHIQUES.shp")
elif sys.argv[1] == "2016":
    rpg = gpd.read_file("data/rpg/2016/RPG_2-0_SHP_2016/RPG/1_DONNEES_LIVRAISON_2016/RPG_2-0_SHP_LAMB93_R84-2016/PARCELLES_GRAPHIQUES.shp")
elif sys.argv[1] == "2018":
    rpg = gpd.read_file("data/rpg/2018/RPG_2-0__SHP_LAMB93_R84-2018_2018-01-15/RPG/1_DONNEES_LIVRAISON_2018/RPG_2-0_SHP_LAMB93_R84-2018/PARCELLES_GRAPHIQUES.shp")
elif sys.argv[1] == "2019":
    rpg = gpd.read_file("data/rpg/2019/RPG_2-0_SHP_LAMB93_R84-2019/RPG/1_DONNEES_LIVRAISON_2019/RPG_2-0_SHP_LAMB93_R84-2019/PARCELLES_GRAPHIQUES.shp")

# -- figure 1 ----
# f, ax = plt.subplots(1)
# reg.plot(ax=ax, alpha=1, facecolor="red")
# pra.plot(ax=ax, alpha=0.5, color="blue", facecolor="green")
# plt.savefig("img/fig1.png")
# plt.clf()
# ----------------

# Aura
aura = reg[reg.code_insee.eq("84")]
aura.loc[15].geometry
pra_aura = pra[pra.geometry.centroid.within(aura.loc[15].geometry)]

# RAM cleaning
del reg
del aura

# -- figure 2 ----
# pra_aura.plot()
# plt.savefig("img/fig2.png")
# plt.clf()
# ----------------

# join rpg/pra
# rpg_pra = gpd.tools.sjoin(rpg, pra, how='left')#[['code_pra', 'name_pra']]
rpg_pra = gpd.tools.sjoin(rpg, pra, op='within', how='left')
# rpg_pra = gpd.tools.sjoin(pra, rpg, how='left')#[['code_pra', 'name_pra']]

# RAM cleaning
del rpg
del pra

# exporting
if not os.path.exists("export"):
    os.makedirs("export")
if not os.path.exists("export/" + sys.argv[1]):
    os.makedirs("export/" + sys.argv[1])
rpg_pra.to_file("export/" + sys.argv[1] + "/rpg_pra" + sys.argv[1] + ".shp", driver="ESRI Shapefile")

# rpg_pra.plot()
# plt.savefig("img/fig3.png")
