import geopandas as gpd 
import matplotlib.pyplot as plt

# PRA
pra = gpd.read_file("data/pra-shapefiles/pra/pra.shp")
pra.geometry = pra.geometry.to_crs(2154)
# print("PRA CRS", pra.crs)
pra = pra.drop([716,717,718,719,720])

# REGIONS
reg = gpd.read_file("data/regions/Regions2016/regions-20161121.shp")
reg.geometry = reg.geometry.to_crs(2154)
# print("REG CRS", reg.crs)
reg = reg.drop([0,1,2,3,4])

# RPG(s)
rpg2015 = gpd.read_file("data/rpg/2015/RPG_2-0_SHP_LAMB93_R84-2015/PARCELLES_GRAPHIQUES.shp")
# rpg.geometry = rpg.geometry.to_crs(4326)
print("RPG CRS", rpg2015.crs)


f, ax = plt.subplots(1)
reg.plot(ax=ax, alpha=1, facecolor="red")
pra.plot(ax=ax, alpha=0.5, color="blue", facecolor="green")
plt.savefig("img/fig1.png")
plt.clf()

aura = reg[reg.code_insee.eq("84")]
aura.loc[15].geometry

pra_aura = pra[pra.geometry.centroid.within(aura.loc[15].geometry)]
# pra.iloc[0].geometry.centroid.within(aura.loc[15].geometry)
pra_aura.plot()
plt.savefig("img/fig2.png")
plt.clf()


rpg_pra = gpd.tools.sjoin(rpg2015, pra, how='left')#[['code_pra', 'name_pra']]
rpg_pra.to_file("export/rpg_pra2015.shp", driver="ESRI Shapefile")


# rpg_pra.plot()
# plt.savefig("img/fig3.png")
