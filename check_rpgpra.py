import geopandas as gpd 
import matplotlib.pyplot as plt
import sys
import os

if sys.argv[1] == "2015":
    rpg_pra = gpd.read_file("export/2015/rpg_pra2015.shp")
elif sys.argv[1] == "2016":
    rpg_pra = gpd.read_file("export/2016/rpg_pra2016.shp")
elif sys.argv[1] == "2018":
    rpg_pra = gpd.read_file("export/2018/rpg_pra2018.shp")
elif sys.argv[1] == "2019":
    rpg_pra = gpd.read_file("export/2019/rpg_pra2019.shp")


# culture maj par pra
results = rpg_pra.groupby(by="name_pra")["CODE_GROUP"].agg(lambda x:x.value_counts().index[0])

if not os.path.exists("results"):
    os.makedirs("results")

compression_opts = dict(method='zip', archive_name='out' + sys.argv[1] + '.csv')  
results.to_csv("results/out" + sys.argv[1] + ".zip", index=True, compression=compression_opts)
os.chdir("results")
os.system("unzip out" + sys.argv[1] + ".zip")
os.chdir("../")